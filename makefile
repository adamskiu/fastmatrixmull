OUT = test
CC = gcc
CXX = g++
CFLAGS =-O3 -fopenmp 
LDFLAGS = -lm -lgomp
SRC = main.cpp

all:
	$(CXX) $(CFLAGS) $(LDFLAGS) $(SRC) -o $(OUT)

clean:
	rm -rf $(OUT)
