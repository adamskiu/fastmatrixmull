/*#################################################################################
 * Cała metoda polega na zrównolegnieniu obliczeń w pętli za pomocą openmp
 * oraz zoptymalizowaniu dostępu do cache poprzez mnożenie drugiej przetransponowanej
 * macierzy
*/
#include <cstdio>
#include <iostream>
#include <omp.h>
#include <string>
#include <stdlib.h>

using namespace std;

const int ROWS1 = 1000;
const int COLS1 = 5000;

const int ROWS2 = 5000;
const int COLS2 = 1000;

void displayMatrix(double **matrix, const int rows, const int colums);


int main()
 {
     // ############ Allocate memory #######################################
     double** table1 = new double*[ROWS1];
     for(int i = 0; i < ROWS1; ++i)
         table1[i] = new double[COLS1];

     double** table2 = new double*[ROWS2];
     for(int i = 0; i < ROWS2; ++i)
         table2[i] = new double[COLS2];

     double** transposedTable2 = new double*[COLS2];
     for(int i = 0; i < COLS2; ++i)
         transposedTable2 [i] = new double[ROWS2];

     double** resultTable = new double*[ROWS1];
     for(int i = 0; i < ROWS1; ++i){
         resultTable[i] = new double[COLS2];
         for (int j = 0; j < COLS2; j++)
             resultTable[i][j] = 0;
     }
     // ############ Allocate memory ####################################### end

     // ############################ Init tables ##############################
    #pragma omp parallel for
     for (int i = 0; i < ROWS1; i++)
      for (int j = 0; j < COLS1; j++)
          table1[i][j]  = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

     #pragma omp parallel for
     for (int i = 0; i < ROWS2; i++)
      for (int j = 0; j < COLS2; j++)
          table2[i][j]  = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

     // ############################ Init tables ############################## end

     /* Przypadek testowy dla dwóch macierzy 3x3
      * Wynik powinien być:
      * 1 2 3
      * 4 5 6
      * 7 8 9
      */
/*
     table1[0][0] = 1;
     table1[0][1] = 2;
     table1[0][2] = 3;
     table1[1][0] = 4;
     table1[1][1] = 5;
     table1[1][2] = 6;
     table1[2][0] = 7;
     table1[2][1] = 8;
     table1[2][2] = 9;

     table2[0][0] = 1;
     table2[0][1] = 0;
     table2[0][2] = 0;
     table2[1][0] = 0;
     table2[1][1] = 1;
     table2[1][2] = 0;
     table2[2][0] = 0;
     table2[2][1] = 0;
     table2[2][2] = 1;
*/
     // Transponowanie drugiej macierzy
     for (int i = 0; i < ROWS2; i++)
         for (int j = 0; j < COLS2; j++)
             transposedTable2[j][i] = table2[i][j];

     cout << "Init done. Multiplying...\n";
     double start, end = 0;

     // ########################## Parallel ###################################
     double sum = 0;
     start = omp_get_wtime();
     #pragma omp parallel for
     for (int i = 0; i < ROWS1; i++)
         for (int j = 0; j < COLS2; j++){
             sum = 0;
             for (int k = 0; k < COLS1; k++){
                 sum += table1[i][k] * transposedTable2[j][k];
             }
             resultTable[i][j] = sum;
         }
     /* standardowy algorytm
     for (int i = 0; i < ROWS1; i++)
         for (int j = 0; j < COLS2; j++)
             for (int k = 0; k < COLS1; k++)
                 resultTable[i][j] += table1[i][k] * table2[k][j];
     */

     end = omp_get_wtime();
     cout << "Work took " << end - start << " seconds (parallel)." << endl;
     // ########################## Parallel ################################### end

     for (int i = 0; i < ROWS1; i++)
         for (int j = 0; j < COLS2; j++)
             resultTable[i][j] = 0;

     // ########################## Not Parallel ###################################
     start = omp_get_wtime();
     for (int i = 0; i < ROWS1; i++)
         for (int j = 0; j < COLS2; j++){
             sum = 0;
             for (int k = 0; k < COLS1; k++){
                 sum += table1[i][k] * transposedTable2[j][k];
             }
             resultTable[i][j] = sum;
         }
     /* standardowy algorytm mnożenia
     for (int i = 0; i < ROWS1; i++)
         for (int j = 0; j < COLS2; j++)
             for (int k = 0; k < COLS1; k++)
                 resultTable[i][j] += table1[i][k] * table2[k][j];
    */
     end = omp_get_wtime();
     cout << "Work took " << end - start << " seconds (not parallel)." << endl;
     // ########################## Not Parallel ################################### end


     //################################ Free memory ##############################
     for(int i = 0; i < ROWS1; ++i){
         delete[]  table1[i];
         delete[]  resultTable[i];
     }
     delete[] table1;
     delete[] resultTable;

     for(int i = 0; i < ROWS2; ++i)
         delete[]  table2[i];
     delete[] table2;

     for(int i = 0; i < COLS2; ++i)
         delete[]  transposedTable2[i];
     delete[] transposedTable2;
     //################################ Free memory ##############################
     return 0;
 }

void displayMatrix(double **matrix, const int rows, const int colums)
{
    for (int i = 0; i < rows; i++){
        for (int j = 0; j < colums; j++){
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }

}
